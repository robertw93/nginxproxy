import requests
import gssapi
from requests_gssapi import HTTPSPNEGOAuth, OPTIONAL

creds = gssapi.Credentials(usage='initiate')
gssapi_auth = HTTPSPNEGOAuth(creds=creds, mutual_authentication=OPTIONAL, opportunistic_auth=True, target_name='HTTP@eypoc-dn0.pocinet.local')
r = requests.get("http://eypoc-dn0.australiasoutheast.cloudapp.azure.com:5002/groups/resources&default&gu", auth=gssapi_auth)
print r.json()

