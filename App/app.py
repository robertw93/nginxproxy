import PolicyController, config, os
from flask import Flask, request, render_template
from flask.json import jsonify
from flask_kerberos import init_kerberos, requires_authentication

system_config = config.Config()
PolicyClient = PolicyController.PolicyClient(system_config)
app = Flask(__name__)

@app.route('/groups/<string:bu>&<string:domain>&<string:confidentiality>')
@requires_authentication
def groups(user, bu, domain, confidentiality):
	labels = PolicyClient.getLabelList(bu, domain, confidentiality)
	groups = PolicyClient.getAllGroupsWithLabels(labels)
	result = { "groups": [] }
	for group in groups:
		result["groups"].append(group[0])
	return jsonify(result)


@app.route('/')
@app.route('/index')
def index():
    return render_template('index.html')

if __name__ == '__main__':
	init_kerberos(app, hostname='rest.pocinet.local')
	app.run(host='0.0.0.0', port='5002')
