# RStudio Connect sample Dockerfile
FROM ubuntu

# R from Ubuntu + necessary tools.
RUN export DEBIAN_FRONTEND=noninteractive && \
    apt-get update && \
    apt-get install -y \
            curl \
            gdebi-core \
            r-base

ARG CONNECT_VERSION=1.6.6.1-4
ARG CONNECT_URL_BASE=https://s3.amazonaws.com/rstudio-connect
ARG CONNECT_PACKAGE=rstudio-connect_${CONNECT_VERSION}_amd64.deb
ARG CONNECT_URL=${CONNECT_URL_BASE}/${CONNECT_PACKAGE}
RUN curl -sL -o rstudio-connect.deb ${CONNECT_URL} && \
    gdebi -n rstudio-connect.deb && \
    rm rstudio-connect.deb

RUN Rscript -e "install.packages('httpuv')"

# Use a remote license server issuing floating licenses
RUN /opt/rstudio-connect/bin/license-manager license-server eypoc-mn0.pocinet.local:8999

# Expose the configured listen port.
EXPOSE 3939

# Launch Connect.
CMD /opt/rstudio-connect/bin/connect \
    --config /etc/rstudio-connect/rstudio-connect.gcfg